import http from "./http";
const { GET, POSTJSON, POSTFROM, DELETE, PUT } = { ...http };
// *******************************分组接口*******************************

// 分组列表
export const getGroups = (params) => GET("/api/v1/groups", params);
// 创建分组
export const createGroups = (params) => POSTFROM("/api/v1/group", params);
// 修改分组
export const updateGroups = (params) =>
  POSTJSON("/api/v1/group/update", params);
// 删除分组
export const deleteGroups = (params) => GET("/api/v1/group/del", params);

// *******************************模型接口*******************************
// 全站模型列表

export const allPatters = (params) =>
  GET("/api/v1/pattern/get_all_patters", params);

// 首页模型列表
export const getIndexModelList = (params) =>
  GET("/api/v1/pattern/indexlist", params);
// 创建模型
export const createModel = (params) =>
  POSTFROM("/api/v1/pattern/creat", params);
// 模型列表
export const getModelList = (params) => GET("/api/v1/pattern/list", params);
// 修改模型
export const updateModel = (params) =>
  POSTFROM("/api/v1/pattern/update", params);
// 删除模型
export const deleteModel = (params) => GET("/api/v1/pattern/del", params);
// 模型数据
export const modeldata = (params) => GET("/api/v1/getmodeldata", params);
// 模型转换
export const modelConvert = (params) => GET("/api/v1/convert", params);
// 模型详情
export const modeldetails = (params) => GET("/api/v1/pattern/getone", params);
// 首页模糊搜索
export const searchList = (params) => GET("api/v1/getsearchList", params);
// 检查是否上传
export const checkUpload = (params) =>
  POSTFROM("/api/v1/pattern/check_upload", params);

// 模型管理大接口
export const modelAdminList = (params) =>
  GET("/api/v1/pattern/admin_list", params);

// 审核标准
export const auditStandard = () => GET("/api/v1/audit/getList");
// 审核详情
export const auditStanDetail = (params) =>
  GET("/api/v1/pattern/getone", params);
// 完整性校验 审核
export const integrityCheck = (params) =>
  GET("/api/v1/pattern/pattern_audit?" + params);

// 模型比对
export const modelCompar = (params) =>
  GET("/api/v1/pattern/pattern_comparison", params);

// 模型审核结果
export const auditStatus = (params) =>
  GET("/api/v1/pattern/audit_status", params);

export const auditStatusPost = (params) =>
  POSTFROM("/api/v1/pattern/audit_status", params);

// 价格发布
export const priceRelea = (params) =>
  GET("/api/v1/pattern/pricing_release?" + params);

// 区域分布
export const patternArea = (params) =>
  GET("/api/v1/pattern/pattern_area", params);

//  版本历史
export const uploadVerison = (params) =>
  GET("/api/v1/patten/get_upload_verison_list", params);

// 比对 同步
export const syncPattern = (params) =>
  GET("/api/v1/pattern/sync_pattern?" + params);

// *******************************系统设置*******************************

export const auditAdd = (params) => POSTFROM("/api/v1/audit/add", params);

// *******************************收藏*******************************

//添加收藏
export const addColletc = (params) => POSTFROM("/api/v1/colletc/add", params);
// 收藏列表
export const getColletcList = (params) =>
  GET("/api/v1/colletc/getList", params);
// 移除收藏
export const delColletc = (id) => GET("/api/v1/colletc/del?id=" + id);

// 模型首页
export const modelIndex = (id) => GET("/api/v1/pattern/model_index");

// *******************************我的模型*******************************

// 我的上传
export const myUploadlist = (params) =>
  GET("/api/v1/pattern/admin_list", params);

export const getmodeldata = (params) => GET("/api/v1/getmodeldata", params);
