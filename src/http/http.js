import axios from "axios";
import { Message } from "element-ui";
import { getToken } from "./cookies";

const request = createAxiosInstance();

console.log(process.env);
function createAxiosInstance() {
  const instance = axios.create({
    // baseURL: process.env.VUE_APP_BASE_URL,
    timeout: 15000,
  });
  // 添加请求拦截器(在发送请求之前做些什么)
  instance.interceptors.request.use((config) => {
    if (getToken()) {
      config.headers["Authorization"] = getToken();
    }
    // if (config.method === 'get') {
    //   config.url = config.url + '?' + Date.parse(new Date().toString())
    // }
    // config.url = config.url.replace(/\|/g, '%7C') // 处理url的特殊字符|
    return config;
  });
  // 添加响应拦截器(对响应数据做点什么)
  instance.interceptors.response.use(
    (response) => {
      const res = response.data;
      //判断失败的code码并作出提示等操作
      if (res.code === 401) {
        Message.error(res.msg);
      }
      return Promise.resolve(response.data);
    },
    (error) => {
      if (error.response.status === 401) {
        Message.error("token失效，请重新登录！");
        setTimeout(() => {
          //   router.push('/login')
        }, 2000);
      } else {
        if (!window.navigator.onLine) {
          Message.warning("网络异常，请检查网络是否正常连接");
        } else if (error.code === "ECONNABORTED") {
          Message.warning("请求超时");
        } else if (error.response.data.code > 500) {
          Message.warning(error.response.data.msg);
        } else {
          Message.warning("服务器异常，请联系管理员");
        }
      }
      return Promise.reject(error); // 将错误继续返回给到具体页面
    }
  );

  return instance;
}

const http = {
  // get
  GET(url, params) {
    return new Promise((resolve, reject) => {
      request
        .get(url, {
          params: params,
        })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // post--json
  POSTJSON(url, data) {
    return new Promise((resolve, reject) => {
      request({
        url,
        method: "post",
        headers: {
          "Content-Type": "application/json",
        },
        data,
      })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  // post--Fromdata
  POSTFROM(url, data) {
    return new Promise((resolve, reject) => {
      request({
        url,
        method: "post",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        },
        transformRequest: [
          function (data) {
            let ret = "";
            for (let it in data) {
              ret +=
                encodeURIComponent(it) +
                "=" +
                encodeURIComponent(data[it]) +
                "&";
            }
            return ret;
          },
        ],
        data,
      })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  DELETE(url, data) {
    return new Promise((resolve, reject) => {
      request({
        url,
        method: "delete",
        data,
      })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  PUT(url, data) {
    return new Promise((resolve, reject) => {
      request({
        url,
        method: "put",
        data,
      })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};

export default http;
