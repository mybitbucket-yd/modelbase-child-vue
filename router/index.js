import Layout from "@/layout/index.vue";

const routes = [
  {
    path: "/",
    name: "dist-vue",
    redirect: "/dist-vue/myModelManage/modelList",
  },
  {
    path: "/dist-vue",
    name: "dist-vue",
    redirect: "/dist-vue/myModelManage/modelList",
  },
  {
    path: "/dist-vue/home",
    name: "home",
    component: () => import("../src/views/home"),
  },
  {
    path: "/dist-vue/myModelManage",
    name: "myModelManage",
    component: Layout,
    meta: {
      title: "模型管理",
    },
    redirect: "/dist-vue/myModelManage/totalStationModel",
    children: [
      {
        path: "/dist-vue/myModelManage/modelList",
        name: "modelList",
        component: () => import("@/views/modelManage/modelList"),
        meta: {
          title: "设备模型",
          ptitle: "模型管理",
        },
      },
      {
        path: "/dist-vue/myModelManage/totalStationModel",
        name: "totalStationModel",
        component: () => import("@/views/modelManage/totalStationModel"),
        meta: {
          title: "全站模型",
          ptitle: "模型管理",
        },
      },
      {
        path: "/dist-vue/myModelManage/ModelDisassembly",
        name: "ModelDisassembly",
        component: () => import("@/views/modelManage/ModelDisassembly"),
        meta: {
          title: "模型拆解",
          ptitle: "模型管理",
        },
      },

      {
        path: "/dist-vue/myModelManage/myUpload",
        name: "myUpload",
        component: () => import("@/views/modelManage/myModel/myUpload"),
        meta: {
          title: "我的上传",
          ptitle: "模型管理",
        },
      },
      {
        path: "/dist-vue/myModelManage/favorites",
        name: "favorites",
        component: () => import("@/views/modelManage/myModel/favorites"),
        meta: {
          title: "收藏夹",
          ptitle: "模型管理",
        },
      },
    ],
  },

  {
    path: "/dist-vue/myModel/shoppingCart",
    name: "shoppingCart",
    component: () => import("@/views/modelManage/myModel/shoppingCart"),
    meta: {
      title: "购物车",
    },
  },
  {
    path: "/dist-vue/myModelManage/myOrder",
    name: "myOrder",
    component: () => import("@/views/modelManage/myModel/myOrder"),
    meta: {
      title: "我的订单",
      ptitle: "购物车",
    },
  },
  {
    path: "/dist-vue/myModelManage/OrderPurchase",
    name: "OrderPurchase",
    component: () => import("@/views/modelManage/myModel/OrderPurchase"),
    meta: {
      title: "订单购买",
      ptitle: "购物车",
    },
  },
  {
    path: "/dist-vue/myModelManage/orderPay",
    name: "orderPay",
    component: () => import("@/views/modelManage/myModel/orderPay"),
    meta: {
      title: "订单支付",
      ptitle: "购物车",
    },
  },
  {
    path: "/dist-vue/myModelManage/myModelEdit",
    name: "myModelEdit",
    component: () => import("@/views/modelManage/myModel/myModelEdit"),
    meta: {
      title: "修改模型",
      ptitle: "模型管理",
    },
  },
  {
    path: "/dist-vue/modelManage/modelupload",
    name: "modelupload",
    component: () => import("@/views/modelManage/modelupload"),
    meta: {
      title: "模型上传",
      ptitle: "模型管理",
      pptitle: "发布管理",
    },
  },
  {
    path: "/dist-vue/modelManage/modelPreview",
    name: "modelPreview",
    component: () => import("@/views/modelManage/modelPreview"),
    meta: {
      title: "模型查看",
      ptitle: "模型管理",
      pptitle: "发布管理",
    },
  },
  {
    path: "/dist-vue/myModel/myModeldetails",
    name: "myModeldetails",
    component: () => import("@/views/modelManage/myModel/myModeldetails"),
    meta: {
      title: "模型详情",
      ptitle: "模型管理",
    },
  },

  {
    path: "/dist-vue/myModel/personalInfo",
    name: "myOrder",
    component: () => import("@/views/modelManage/myModel/personalInfo"),
    meta: {
      title: "个人信息",
    },
  },

  {
    path: "/dist-vue/modelManage/modelVerification",
    name: "modelVerification",
    component: () => import("@/views/modelManage/modelVerification"),
    meta: {
      title: "发布管理",
    },
  },
  {
    path: "/dist-vue/modelManage/modeldetails",
    name: "modeldetails",
    component: () => import("@/views/modelManage/modeldetails"),
    meta: {
      title: "模型预览",
      ptitle: "发布管理",
    },
  },
  {
    path: "/dist-vue/modelManage/regDistribution",
    name: "regDistribution",
    component: () => import("@/views/modelManage/regDistribution"),
    meta: {
      title: "区域分布",
      ptitle: "发布管理",
    },
  },
  {
    path: "/dist-vue/modelManage/modelEdit",
    name: "modelEdit",
    component: () => import("@/views/modelManage/modelEdit"),
    meta: {
      title: "模型修改",
      ptitle: "发布管理",
    },
  },
  {
    path: "/dist-vue/modelManage/integrityCheck",
    name: "integrityCheck",
    component: () => import("@/views/modelManage/integrityCheck"),
    meta: {
      title: "完整性校验",
      ptitle: "发布管理",
    },
  },
  {
    path: "/dist-vue/modelManage/comCorrect",
    name: "comCorrect",
    component: () => import("@/views/modelManage/comCorrect"),
    meta: {
      title: "比对修正",
      ptitle: "发布管理",
    },
  },
  {
    path: "/dist-vue/modelManage/modelRelease",
    name: "modelRelease",
    component: () => import("@/views/modelManage/modelRelease"),
    meta: {
      title: "模型发布",
      ptitle: "发布管理",
    },
  },
  {
    path: "/dist-vue/modelManage/versionUpdate",
    name: "versionUpdate",
    component: () => import("@/views/modelManage/versionUpdate"),
    meta: {
      title: "版本更新",
      ptitle: "发布管理",
    },
  },
  {
    path: "/dist-vue/systemSetting/systemSetting",
    name: "systemSetting",
    component: () => import("@/views/systemSetting/systemSetting"),
    meta: {
      title: "系统设置",
    },
  },
  {
    path: "/dist-vue/memberManage",
    name: "memberManage",
    component: () => import("@/views/memberManage/memberManage.vue"),
    meta: {
      title: "会员管理",
    },
  },
];

let routers = getRouters(routes);
let MODELBASEROUTES = sessionStorage.getItem("MODELBASEROUTES");
if (MODELBASEROUTES) {
  sessionStorage.removeItem("MODELBASEROUTES");
}
sessionStorage.setItem("MODELBASEROUTES", JSON.stringify(routers));

function getRouters(router) {
  let routersArr = [];
  getRoutersByfor(router);
  function getRoutersByfor(router) {
    router.forEach((item) => {
      let namepath = { name: "", path: "" };
      if (item.meta && item.meta.title) {
        namepath.name = item.meta.title;
        namepath.path = item.path;
        routersArr.push(namepath);
      }
      if (item.children && item.children.length > 0) {
        getRoutersByfor(item.children);
      }
    });
  }
  return routersArr;
}

export default routes;
