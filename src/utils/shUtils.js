import Vue from "vue";

let modelBaseRouteStr = null;
let modelBaseGroupsStr = null;
let modelBaseRoutes = null;
let modelBaseGroups = null;
let isTrue = false;

let time = setInterval(() => {
  isTrue = getInfo();
  if (isTrue) {
    clearInterval(time);
  }
}, 1000);

function getInfo() {
  modelBaseRouteStr = sessionStorage.getItem("MODELBASEROUTES");
  modelBaseGroupsStr = sessionStorage.getItem("MODELBASEGROUPS");
  modelBaseRoutes = modelBaseRouteStr ? JSON.parse(modelBaseRouteStr) : [];
  modelBaseGroups = modelBaseGroupsStr ? JSON.parse(modelBaseGroupsStr) : [];
  //   console.log(modelBaseRoutes, modelBaseGroups, "dddddd");
  if (modelBaseRouteStr && modelBaseGroupsStr) return true;
  return false;
}

// let { fileTypeList } = { ...modelBaseGroups };

export function getPathByName(name) {
  let path = null;
  modelBaseRoutes.forEach((item) => {
    if (item.name == name) {
      path = item.path;
    }
  });
  return path;
}

export function pushHandler(name, Vue, params) {
  console.log(name, params);
  let path = getPathByName(name);
  //   console.log(name, path, params, "pushHandler");
  if (params) {
    Vue.$router.push({
      path: path,
      query: { query: JSON.stringify(params) },
    });
  } else {
    Vue.$router.push({ path: path });
  }
}
// // 模型用途
// Vue.prototype.getFileTypeByNum = function (type) {
//   let key = null;
//   fileTypeList.forEach((item) => {
//     if (item.id == type) {
//       key = item.name;
//     }
//   });
//   return key;
// };
