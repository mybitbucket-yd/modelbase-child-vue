// 全局组件
import Vue from "vue";

import Breadcrumb from "./breadcrumb.vue";
import Table from "./table.vue";
import TabBar from "./tabBar.vue";
import Divider from "./divider.vue";
import dividerSecond from "./dividerSecond.vue";
import TaBarSecond from "./taBarSecond.vue";

Vue.component("Breadcrumb", Breadcrumb);
Vue.component("Table", Table);
Vue.component("TabBar", TabBar);
Vue.component("Divider", Divider);
Vue.component("DivSecond", dividerSecond);
Vue.component("TaBarSecond", TaBarSecond);
