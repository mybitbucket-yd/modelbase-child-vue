import "./public-path";
import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";
import routes from "../router";
import uploader from "vue-simple-uploader";
import Element from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import md5 from "js-md5";
import "./utils/flexible";
import "./components/index";

// import Editor from "bin-ace-editor";
// // require("brace/ext/emmet"); // 如果是lang=html时需引入
// require("brace/ext/language_tools"); // language extension

// require("brace/mode/json");
// require("brace/snippets/json");
// require("brace/theme/vibrant_ink");
// // 注册组件后即可使用
// Vue.component(Editor.name, Editor);
// console.log(Editor.name, "Editor.name");

Vue.prototype.$md5 = md5;

Vue.config.productionTip = false;

let router = null;
let instance = null;

function render(props = {}) {
  const { container } = props;
  Vue.use(VueRouter);
  Vue.use(uploader);
  Vue.use(Element);
  router = new VueRouter({
    // base: window.__POWERED_BY_QIANKUN__ ? "/dist-vue/" : "/",
    mode: "hash",
    routes,
  });

  instance = new Vue({
    router,
    // store,
    render: (h) => h(App),
  }).$mount(container ? container.querySelector("#childTwo") : "#childTwo");
}

// 独立运行时
if (!window.__POWERED_BY_QIANKUN__) {
  render();
}

export async function bootstrap() {
  console.log("[vue] vue app bootstraped");
}
export async function mount(props) {
  console.log("子应用二挂载", props);
  render(props);
}
export async function unmount() {
  instance.$destroy();
  instance.$el.innerHTML = "";
  instance = null;
  router = null;
}

export async function update() {
  instance.$destroy();
  instance.$el.innerHTML = "";
  instance = null;
  router = null;
}
