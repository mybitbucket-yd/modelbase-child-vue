import * as THREE from "three";
import { LineMaterial } from "three/examples/jsm/lines/LineMaterial";
import { LineGeometry } from "three/examples/jsm/lines/LineGeometry";
import { Line2 } from "three/examples/jsm/lines/Line2.js";

import PEBaseScene from "./PEBaseScene";

import axios from "axios";

var CancelToken = axios.CancelToken;

export default class SceneController extends PEBaseScene {
  /**
   *保存当前正在发起的所有的网络请求
   *
   * @memberof SceneController
   */
  allRequest = new Map();

  /**
   * 加载整个FBX模型
   * @param {String} source model_id+'|'+modle_verison_id
   */
  LoadMesh(source) {
    if (source == null || source === "") {
      console.error("FBX路径不能为空!");
    } else {
      this.LoadMeshWithNodeList(source, this.root, null);
    }
  }
  /**
   *添加节点
   * @param {String} parentSource 父节点所属Source  model_id+'|'+modle_verison_id
   * @param {String} ParentNodeName 父节点名称
   * @param {String} addSource 添加的FBX路径 model_id+'|'+modle_verison_id
   * @memberof SceneController
   */
  AddMesh(parentSource, ParentNodeName, addSource) {
    //判断source以及ParentNodeName是否存在
    //获取文件名称
    let fileName = parentSource;
    if (this.allMesh.has(fileName + "-" + ParentNodeName)) {
      let nodeObject = this.allMesh.get(fileName + "-" + ParentNodeName);
      this.LoadMeshWithNodeList(addSource, nodeObject.mesh, null);
    } else {
      console.error(parentSource + ParentNodeName + "节点不存在");
    }
  }

  /**
   * 卸载整个场景
   */
  ClearScene() {
    //停止当前正在发起的所有的请求
    for (const key of this.allRequest.keys()) {
      let reqList = this.allRequest.get(key);
      reqList.forEach((res) => {
        res();
        res = null;
      });
    }
    this.allRequest = new Map();
    this.allMesh = new Map();
    this.Remove(this.root.children);
  }

  /**
   * 卸载整个FBX
   * @param {String} source FBX路径地址 model_id+'|'+modle_verison_id
   */
  RemoveMesh(source) {
    //停止当前source下的所有的网络请求
    if (this.allRequest.has(source)) {
      let reqList = this.allRequest.get(source);
      reqList.forEach((res) => {
        res();
        res = null;
      });
      this.allRequest.delete(source);
    }
    let allRemoveMesh = [];
    let allkey = [];
    for (const key of this.allMesh.keys()) {
      let nodeObject = this.allMesh.get(key);
      if (nodeObject.source == source) {
        allRemoveMesh.push(nodeObject.mesh);
        allkey.push(key);
      }
    }
    allkey.forEach((key) => {
      this.allMesh.delete(key);
    });
    this.Remove(allRemoveMesh);
  }

  /**
   * 卸载Node列表
   * @param {Array} source FBX中的node节点列表 model_id+'|'+modle_verison_id
   * @param {Array} nodeNameList FBX中的node节点列表
   */
  RemoveNodeList(source, nodeNameList) {
    let self = this;
    let allRemoveMesh = [];
    let allkey = [];
    //获取文件名称
    let fileName = source;
    nodeNameList.forEach((nodeName) => {
      if (self.allMesh.has(fileName + "-" + nodeName)) {
        let nodeObject = self.allMesh.get(fileName + "-" + nodeName);
        allRemoveMesh.push(nodeObject.mesh);
        allkey.push(fileName + "-" + nodeName);
      } else {
        console.error(source + nodeName + "节点不存在");
      }
    });

    allkey.forEach((key) => {
      this.allMesh.delete(key);
    });
    if (allRemoveMesh.length == 0) {
      console.error("暂无可以删除的节点");
      return;
    }
    this.Remove(allRemoveMesh);
  }

  /**
   *根据NodeTree初始化场景
   *
   * @param {*} source
   * @param {*} parentMesh
   * @param {*} nodeTree
   * @memberof SceneController
   */
  LoadMeshWithNodeList(source, parentMesh, nodeList) {
    let self = this;
    if (source == null || source === "") {
      console.error("FBX路径不能为空!");
    } else {
      if (nodeList == null) {
        self.LoadMeshData(source, parentMesh, null);
      } else {
        nodeList.forEach((nodeName) => {
          self.LoadMeshData(source, parentMesh, nodeName);
        });
      }
    }
  }
  /**
   *加载模型数据
   *
   * @param {*} source
   * @param {*} parentNodeMesh
   * @param {*} nodeName
   * @memberof SceneController
   */
  LoadMeshData(source, parentNodeMesh, loadNodeName) {
    let self = this;
    //解析source
    let array = source.split("|");
    if (array.length == 0 || array.length != 2) {
      console.error("source存在异常");
      return;
    }
    let model_id = array[0];
    let modle_verison_id = array[1];
    let postParam = {
      model_id: model_id,
      modle_verison_id: modle_verison_id,
      target: "all",
    };
    axios
      .request({
        url: this.sceneConfig.url,
        method: "get",
        params: postParam,
        cancelToken: new CancelToken(function executor(c) {
          if (self.allRequest.has(source)) {
            self.allRequest.get(source).push(c);
          } else {
            self.allRequest.set(source, [c]);
          }
        }),
      })
      .then((res) => {
        if (res.data.msg !== "成功" || res.data.data.status != null) {
          console.error(res.data.data.data.describe);
        } else {
          //获取文件名称
          let fileName = source;
          let nodeTree = res.data.data.nodetree;
          if (nodeTree == null || nodeTree.length == 0) {
            console.error("该FBX下没有任何Node节点");
          } else {
            //遍历出nodeTree中包含的所有的nodeName节点
            //筛选出本次加载的nodeTree中新增的node节点
            let newNodeList = [];
            nodeTree.forEach((nodeObject) => {
              if (!self.allMesh.has(fileName + "-" + nodeObject.name)) {
                self.allMesh.set(fileName + "-" + nodeObject.name, {
                  nodeName: nodeObject.name,
                  source: source,
                  type: nodeObject.type,
                  isAdd: false,
                  mt: nodeObject.mt,
                });
                newNodeList.push(fileName + "-" + nodeObject.name);
              }
            });

            if (newNodeList.length == 0) {
              console.error("没有任何可以添加的Node节点");
              return;
            }
            let allInitNumber = 0;
            let ISEnd = (number) => {
              allInitNumber += number;
              if (newNodeList.length == allInitNumber) {
                console.log(
                  "加载完成" + newNodeList.length + "--" + allInitNumber
                );
                //启动相机定位
                self.LookatScene();
              } else {
                console.log(
                  "PEIEN:当前加载进度" +
                    newNodeList.length +
                    "--" +
                    allInitNumber
                );
              }
            };
            let allGroupNode = [];
            let allDreawMesh = [];
            //将nodeTree进行分组归类
            for (let index = 0; index < newNodeList.length; index++) {
              const nodeObject = self.allMesh.get(newNodeList[index]);
              if (nodeObject.source == source) {
                //判断类型是否需要获取NodeName信息
                if (
                  nodeObject.type === "YDMTNode" ||
                  nodeObject.type === "YDGroup"
                ) {
                  allGroupNode.push(nodeObject);
                } else {
                  allDreawMesh.push(nodeObject);
                }
              }
            }

            for (let index = 0; index < allGroupNode.length; index++) {
              const nodeObject = allGroupNode[index];
              self.InitMeshWithData(null, fileName + "-" + nodeObject.nodeName);
            }

            self.InitNodeTree(nodeTree, parentNodeMesh, fileName);
            ISEnd(allGroupNode.length);
            for (let index = 0; index < allDreawMesh.length; index++) {
              const nodeObject = allDreawMesh[index];
              //如果nodeName对应的path中已经存在加载失败的节点,则不在继续请求该path中的其他节点
              let nodeNamePostParam = {
                model_id: model_id,
                modle_verison_id: modle_verison_id,
                node_name: nodeObject.nodeName,
                target: "all",
              };
              axios
                .request({
                  url: this.sceneConfig.url,
                  method: "get",
                  params: nodeNamePostParam,
                  cancelToken: new CancelToken(function executor(c) {
                    if (self.allRequest.has(source)) {
                      self.allRequest.get(source).push(c);
                    } else {
                      self.allRequest.set(source, [c]);
                    }
                  }),
                })
                .then((res) => {
                  if (res.data.msg !== "成功" || res.data.data.status != null) {
                    console.error(
                      source +
                        "|" +
                        nodeNamePostParam.node_name +
                        "节点获取数据失败"
                    );
                    ISEnd(1);
                  } else {
                    self.InitMeshWithData(
                      res.data.data,
                      fileName + "-" + nodeObject.nodeName
                    );
                    //获取父节点
                    let nodeList = nodeObject.nodeName.split("/");
                    nodeList.splice(nodeList.length - 1, 1);
                    let parentNodeName = nodeList.join("/");
                    if (self.allMesh.has(fileName + "-" + parentNodeName)) {
                      let parentNodeObject = self.allMesh.get(
                        fileName + "-" + parentNodeName
                      );
                      parentNodeObject.mesh.add(nodeObject.mesh);
                      nodeObject.isAdd = true;
                      if (nodeObject.mt != null) {
                        let matrix = new THREE.Matrix4();
                        matrix.set(...nodeObject.mt);
                        nodeObject.mesh.applyMatrix4(matrix);
                      }

                      ISEnd(1);
                    } else {
                      console.error("节点查找失败:" + parentNodeName);
                    }
                  }
                })
                .catch((err) => {
                  console.error(err);
                  ISEnd(1);
                });
            }
          }
        }
      })
      .catch((err) => {
        console.error(err);
      });
  }
  /**
   *初始化节点数
   *
   * @memberof SceneController
   */
  InitNodeTree(nodeTree, parentNodeMesh, fileName) {
    let self = this;
    let lastNodeName_Path = "";
    //遍历整个路径,去按照路径顺序往场景中添加Mesh
    nodeTree.forEach((nodeObject) => {
      let parentMesh = parentNodeMesh;
      let nodeList = nodeObject.name.split("/");
      for (let index = 0; index < nodeList.length; index++) {
        if (nodeList[index] == "") {
          continue;
        }
        const nodeName_Path = lastNodeName_Path + "/" + nodeList[index];
        lastNodeName_Path = nodeName_Path;
        if (self.allMesh.has(fileName + "-" + nodeName_Path)) {
          let nodeObject_Path = self.allMesh.get(
            fileName + "-" + nodeName_Path
          );
          if (
            !nodeObject_Path.isAdd &&
            nodeObject_Path.mesh != null &&
            parentMesh != null
          ) {
            parentMesh.add(nodeObject_Path.mesh);
            nodeObject_Path.isAdd = true;

            if (nodeObject_Path.mt != null) {
              // if (nodeObject_Path.nodeName==='/0/1/62/63/64/86') {
              //   nodeObject_Path.mesh.position.set(0.000000,-2851.919189,-378.619293);
              //   nodeObject_Path.mesh.scale.set(1.000000,1.000000,1.000000);
              //   nodeObject_Path.mesh.quaternion.set(0.500000,0.500000,0.500000,0.500000);
              // }else{
              //   let matrix = new THREE.Matrix4();
              //   matrix.set(...nodeObject_Path.mt);
              //   nodeObject_Path.mesh.applyMatrix4(matrix);
              // }
              let matrix = new THREE.Matrix4();
              matrix.set(...nodeObject_Path.mt);
              nodeObject_Path.mesh.applyMatrix4(matrix);
            }
          }
          parentMesh = nodeObject_Path.mesh;
        } else {
          break;
        }
      }
      lastNodeName_Path = "";
    });
  }
  /**
   *根据模型数据初始化模型
   *
   * @memberof PEBaseScene
   */
  InitMeshWithData(data, nodeName) {
    if (this.allMesh.has(nodeName)) {
      let nodeObject = this.allMesh.get(nodeName);
      if (nodeObject.type === "YDMTNode" || nodeObject.type === "YDGroup") {
        let mesh = new THREE.Group();
        mesh.name = nodeName;
        nodeObject.mesh = mesh;
        //设置矩阵信息
        // if (nodeObject.mt != null) {
        //   let matrix = new THREE.Matrix4();
        //   matrix.set(...nodeObject.mt);
        //   mesh.applyMatrix4(matrix);
        //   console.log('PEIEN-' + nodeName + '初始化矩阵信息')
        // }
      } else if (nodeObject.type === "YDDrawable") {
        //判断必备参数信息
        if (data.drawarray == null || data.vertex == null) {
          return;
        }

        //默认透明度
        let opacity = 1.0;
        data.drawarray.forEach((drawData) => {
          //几何体
          if (drawData.type === "4") {
            //判断需要渲染的几何体类型
            let geometry = new THREE.BufferGeometry();

            geometry.attributes.position = new THREE.BufferAttribute(
              new Float32Array(data.vertex),
              3
            );
            console.log("PEIEN-" + nodeName + "初始化顶点信息");

            //法线
            if (data.normal != null) {
              geometry.attributes.normal = new THREE.BufferAttribute(
                new Float32Array(data.normal),
                3
              );
              console.log("PEIEN-" + nodeName + "初始化法线信息");
            } else {
              console.error("暂无normal数据");
            }

            //uv坐标
            if (data.uv != null) {
              geometry.attributes.uv = new THREE.BufferAttribute(
                new Float32Array(data.uv),
                2
              );
              console.log("PEIEN-" + nodeName + "初始化UV信息");
            } else {
              console.error("暂无UV数据");
            }

            if (drawData.data != null) {
              //顶点索引
              geometry.index = new THREE.BufferAttribute(
                new Uint16Array(drawData.data),
                1
              );
            }

            //解析颜色
            let colorArray = [];
            if (data.color != null) {
              if (data.color.length == 4) {
                //表示只有一个色值
                for (
                  let index = 0;
                  index < geometry.attributes.position.count;
                  index++
                ) {
                  colorArray.push(data.color[0] / 255.0);
                  colorArray.push(data.color[1] / 255.0);
                  colorArray.push(data.color[2] / 255.0);
                }
                geometry.attributes.color = new THREE.BufferAttribute(
                  new Float32Array(colorArray),
                  3
                );
                opacity = data.color[3] / 255.0;
              } else {
                for (let index = 0; index < data.color.length; index++) {
                  if ((index + 1) % 4 != 0) {
                    colorArray.push(data.color[index]);
                  } else {
                    opacity = data.color[index] / 255.0;
                  }
                }
                colorArray = data.color;
                geometry.attributes.color = new THREE.BufferAttribute(
                  new Float32Array(colorArray),
                  3
                );
              }
            }
            //初始化材质对象
            let material = new THREE.MeshPhongMaterial({
              vertexColors: THREE.VertexColors,
              transparent: true,
              opacity: opacity,
              side: THREE.DoubleSide,
            });

            if (data.image != null && data.image.length != 0) {
              //初始化纹理数据
              let imageObject = data.image[0];
              let imageData = new Uint8Array(imageObject.data);
              let texture = new THREE.DataTexture(
                imageData,
                imageObject.width,
                imageObject.height,
                THREE.RGBFormat
              );
              if (data.material.texture != null) {
                this.SetMaterialWithConfig(data.material.texture, texture);
                if (data.material.texture.wrapS != null) {
                  switch (data.material.texture.wrapS) {
                    case 1:
                      texture.wrapS = THREE.ClampToEdgeWrapping;
                      break;
                    case 2:
                      texture.wrapS = THREE.RepeatWrapping;
                      break;
                    case 3:
                      texture.wrapS = THREE.MirroredRepeatWrapping;
                      break;

                    default:
                      break;
                  }
                }

                if (data.material.texture.wrapT != null) {
                  switch (data.material.texture.wrapT) {
                    case 1:
                      texture.wrapT = THREE.ClampToEdgeWrapping;
                      break;
                    case 2:
                      texture.wrapT = THREE.RepeatWrapping;
                      break;
                    case 3:
                      texture.wrapT = THREE.MirroredRepeatWrapping;
                      break;

                    default:
                      break;
                  }
                }
              }
              material.map = texture;
              console.log("PEIEN-" + nodeName + "初始化纹理信息");
            } else {
              console.error("暂无image数据");
            }

            if (data.material != null && data.material.code == 0) {
              this.SetMaterialWithConfig(data.material.geometry, material);
            } else {
              console.error("暂无材质数据");
            }

            let mesh = new THREE.Mesh(geometry, material);
            mesh.name = nodeName;
            nodeObject.mesh = mesh;
            //设置矩阵
            // if (data.matrix != null) {
            //   let matrix = new THREE.Matrix4();
            //   matrix.set(...data.matrix);
            //   mesh.applyMatrix4(matrix);
            //   console.log('PEIEN-' + nodeName + '初始化矩阵信息')
            // }
          }
          //点
          if (drawData.type === "0") {
            let geometry = new THREE.BufferGeometry();
            //获取点云的顶点数据
            let pointArray = [];
            let newColorArray = [];
            let colorArray = [];
            let isGetColor = false;
            if (data.color != null) {
              if (data.color.length == 4) {
                //表示只有一个色值
                if (drawData.data != null) {
                  for (let index = 0; index < drawData.data.length; index++) {
                    colorArray.push(data.color[0] / 255.0);
                    colorArray.push(data.color[1] / 255.0);
                    colorArray.push(data.color[2] / 255.0);
                  }
                } else {
                  for (let index = 0; index < data.vertex.length / 3; index++) {
                    colorArray.push(data.color[0] / 255.0);
                    colorArray.push(data.color[1] / 255.0);
                    colorArray.push(data.color[2] / 255.0);
                  }
                }
                opacity = data.color[3] / 255.0;
              } else {
                isGetColor = true;
                for (let index = 0; index < data.color.length; index++) {
                  if ((index + 1) % 4 != 0) {
                    newColorArray.push(data.color[index]);
                  } else {
                    opacity = data.color[index] / 255.0;
                  }
                }
              }
            }
            if (drawData.data != null) {
              drawData.data.forEach((index) => {
                let startIndex = index * 3;
                pointArray.push(data.vertex[startIndex]);
                if (isGetColor) {
                  colorArray.push(newColorArray[startIndex] / 255.0);
                }
                startIndex += 1;
                pointArray.push(data.vertex[startIndex]);
                if (isGetColor) {
                  colorArray.push(newColorArray[startIndex] / 255.0);
                }
                startIndex += 1;
                pointArray.push(data.vertex[startIndex++]);
                if (isGetColor) {
                  colorArray.push(newColorArray[startIndex] / 255.0);
                }
              });
              geometry.attributes.position = new THREE.BufferAttribute(
                new Float32Array(pointArray),
                3
              );
            } else {
              geometry.attributes.position = new THREE.BufferAttribute(
                new Float32Array(data.vertex),
                3
              );
            }

            //初始化点云材质
            let material = new THREE.PointsMaterial({
              vertexColors: THREE.VertexColors,
              transparent: true,
              opacity: opacity,
            });
            if (data.material != null) {
              this.SetMaterialWithConfig(data.material.point, material);
            } else {
              console.error("暂无材质数据");
            }

            //初始化顶点颜色
            //初始化顶点颜色
            geometry.attributes.color = new THREE.BufferAttribute(
              new Float32Array(colorArray),
              3
            );

            let mesh = new THREE.Points(geometry, material);
            this.root.add(mesh);
            nodeObject.mesh = mesh;
          }
          //线段
          if (drawData.type === "1") {
            //获取点云的顶点数据
            let pointArray = [];
            let newColorArray = [];
            let colorArray = [];
            let isGetColor = false;
            if (data.color != null) {
              if (data.color.length == 4) {
                //表示只有一个色值
                if (drawData.data != null) {
                  for (let index = 0; index < drawData.data.length; index++) {
                    colorArray.push(data.color[0] / 255.0);
                    colorArray.push(data.color[1] / 255.0);
                    colorArray.push(data.color[2] / 255.0);
                  }
                } else {
                  for (let index = 0; index < data.vertex.length / 3; index++) {
                    colorArray.push(data.color[0] / 255.0);
                    colorArray.push(data.color[1] / 255.0);
                    colorArray.push(data.color[2] / 255.0);
                  }
                }
                opacity = data.color[3] / 255.0;
              } else {
                isGetColor = true;
                for (let index = 0; index < data.color.length; index++) {
                  if ((index + 1) % 4 != 0) {
                    newColorArray.push(data.color[index]);
                  } else {
                    opacity = data.color[index] / 255.0;
                  }
                }
              }
            }
            if (drawData.data != null) {
              drawData.data.forEach((index) => {
                let startIndex = index * 3;
                pointArray.push(data.vertex[startIndex]);
                if (isGetColor) {
                  newColorArray.push(data.color[startIndex] / 255.0);
                }
                startIndex += 1;
                pointArray.push(data.vertex[startIndex]);
                if (isGetColor) {
                  newColorArray.push(data.color[startIndex] / 255.0);
                }
                startIndex += 1;
                pointArray.push(data.vertex[startIndex++]);
                if (isGetColor) {
                  newColorArray.push(data.color[startIndex] / 255.0);
                }
              });
            }

            //初始化材
            let material = new LineMaterial({
              transparent: true,
              vertexColors: THREE.VertexColors,
              opacity: opacity,
            });
            if (data.material != null) {
              this.SetMaterialWithConfig(data.material.line, material);
            } else {
              console.error("暂无材质数据");
            }

            let geometry = new LineGeometry();
            if (drawData.data != null) {
              geometry.setPositions(pointArray);
            } else {
              geometry.setPositions(data.vertex);
            }
            geometry.setColors(colorArray);
            material.resolution.set(window.innerWidth, window.innerHeight);
            let mesh = new Line2(geometry, material);
            this.root.add(mesh);
            nodeObject.mesh = mesh;
          }
        });
      } else {
        console.error(data.type + "未定义");
      }
    }
  }
  /**
   *相机定位场景视角
   *
   * @memberof SceneController
   */
  LookatScene() {
    if (this.root.children.length == 0) {
      return;
    }
    let box3 = new THREE.Box3();
    box3.expandByObject(this.root);
    let centerPosition = box3.getCenter();
    var sphere = new THREE.Sphere();
    box3.getBoundingSphere(sphere);
    //设置场景的中心点为包围球的中心点
    this.controls.target = centerPosition.clone();
    //计算相机当前位置与包围盒两倍半径俯视角度45度的坐标
    let center = new THREE.Vector3()
      .addVectors(box3.max, box3.min)
      .multiplyScalar(0.5);
    let radio = new THREE.Vector3().subVectors(box3.max, box3.min).length() / 2;
    let vec = new THREE.Vector3().subVectors(
      this.camera.position,
      centerPosition
    );
    let vecxy = new THREE.Vector3(vec.x, vec.y, 0);
    let vecxy90 = new THREE.Vector3(-vec.y, vec.x, 0);
    vecxy90.normalize();
    vecxy.applyAxisAngle(vecxy90, 45);
    vecxy.z = Math.abs(vecxy.z);
    vecxy.normalize();
    vecxy.multiplyScalar(radio * sphere.radius < 1 ? 3 : radio);
    center.add(vecxy);
    this.camera.position.copy(center);
    this.controls.update();
  }
  /**
   *根据材质信息动态映射材质属性
   *
   * @param {*} materialData
   * @param {*} material
   * @memberof SceneController
   */
  SetMaterialWithConfig(materialData, material) {
    if (materialData == null) {
      return;
    }
    Object.keys(materialData).forEach((key) => {
      if (key.indexOf("set_") != -1) {
        material[key.replace("set_", "")].set(...materialData[key]);
      } else {
        material[key] = materialData[key];
      }
    });
  }
}
