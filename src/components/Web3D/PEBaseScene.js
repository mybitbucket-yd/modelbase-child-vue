
import * as THREE from "three";
import * as Stats from "three-stats";
import * as OrbitControls from "three-orbitcontrols";


export default class PEBaseScene {

  /**
   *场景对象
   *
   * @memberof PEBaseScene
   */
  scene;
  /**
   *场景的根节点模型,方便统一控制场景模型
   *
   * @memberof PEBaseScene
   */
  root;
  /**
   *页面中用于绘制Canvas的DOM节点对象
   *
   * @memberof PEBaseScene
   */
  rootDiv;
  /**
   *用于渲染场景的Canvas对象
   *
   * @memberof PEBaseScene
   */
  rootCanvas;
  /**
   *默认相机对象
   *
   * @memberof PEBaseScene
   */
  camera;
  /**
   *默认渲染器
   *
   * @memberof PEBaseScene
   */
  renderer;
  /**
   *默认控制器
   *
   * @memberof PEBaseScene
   */
  controls;
  /**
   *性能检测组件
   *
   * @memberof PEBaseScene
   */
  _stats;
  /**
   *平行光
   *
   * @memberof PEBaseScene
   */
  _directionalLight = null;
  /**
   *环境光
   *
   * @memberof PEBaseScene
   */
  _ambientLight = null;

  /**
   *保存当前场景中所有的Mesh对象与NodeName直接的对应关系
   *
   * @memberof PEBaseScene
   */
  allMesh = new Map();
  /**
   *场景配置文件
   *
   * @memberof PEBaseScene
   */
  sceneConfig = null;
  /**
   *场景初始化
   *
   * @param {*} dom
   * @param {*} [sceneParam={}]
   * @param {*} [endBlock=null]
   * @memberof PEBaseScene
   */
  Init (dom, sceneConfig) {
    this.rootDiv = dom;
    this.sceneConfig = sceneConfig;
    this.InitData();
    this.InitScene();
    this.InitLight();
    this.InitCamera();
    this.InitRender();
    this.InitController();
    this.InitOther();
    this.InitHelp();
    this.LoadingEnd();
    this.SceneRender();
  }

  /**
   *初始化场景默认需要的数据
   *
   * @memberof PEBaseScene
   */
  InitData () {
    //初始化Three.js 显示容器
    let myCanvas = document.createElement("CANVAS");
    myCanvas.style = "width:100%;height:100%;display:block;"
    this.rootDiv.appendChild(myCanvas);
    this.rootCanvas = myCanvas;
  }


  /**
   *设置默认场景
   *
   * @memberof PEBaseScene
   */
  InitScene () {
    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(this.sceneConfig.backgroundColor)
    //所有Mesh的父容器
    this.root = new THREE.Object3D();
    this.scene.add(this.root);
  }
  /**
   *设置默认灯光
   *
   * @memberof PEBaseScene
   */
  InitLight () {
    //平行光
    {
      let light = new THREE.DirectionalLight('#EEFOF6');
      this._directionalLight = light;
      this._directionalLight.position.set(0, 41.497, 47.424);
      this.scene.add(light);
    }
    //环境光
    {
      let ambientLight = new THREE.AmbientLight('#000003');
      this._ambientLight = ambientLight;
      this.scene.add(ambientLight);
    }
    //半球光
    {
      let hemisphereLight = new THREE.HemisphereLight(0x5C6470, 0xBAC5D9, 0.5);
      this.hemisphereLight = hemisphereLight;
      this.hemisphereLight.position.set(0, 163.963, 0);
      this.scene.add(hemisphereLight);
    }
  }
  /**
   *设置默认相机
   *
   * @memberof PEBaseScene
   */
  InitCamera () {
    this.camera = new THREE.PerspectiveCamera(
      45,
      this.rootDiv.offsetWidth / this.rootDiv.offsetHeight,
      0.01,
      200000
    );
    this.camera.position.set(1.051343428006883,11.276206691021581,27.017994575184293);
    this.camera.up.set(0,0,1);
    this.camera.lookAt(this.root.position);
    this.camera.updateMatrixWorld();
    this.camera.updateProjectionMatrix();
  }

  /**
   *初始化渲染器
   *
   * @memberof PEBaseScene
   */
  InitRender () {
    this.renderer = new THREE.WebGLRenderer({ canvas: this.rootCanvas, antialias: true, logarithmicDepthBuffer: true });
    const pixelRatio = window.devicePixelRatio;
    this.renderer.setPixelRatio(pixelRatio);
    const width = this.rootDiv.offsetWidth;
    const height = this.rootDiv.offsetHeight;
    this.renderer.setSize(
      width,
      height,
      false
    ); //设置渲染区域尺寸
    this.renderer.shadowMap.enable = false; // 阴影
    this.renderer.gammaOutput = true;
    this.renderer.gammaFactor = 2.2;   //电脑显示屏的gammaFactor为2.2    
  }
  /**
   *初始化控制器
   *
   * @memberof PEBaseScene
   */
  InitController () {
    this.controls = new OrbitControls(
      this.camera,
      this.renderer.domElement
    );
  }
  /**
   *初始化一些系统性的事件监听之类的操作
   *
   * @memberof PEBaseScene
   */
  InitOther () {
    //监听浏览器窗口变化,用于3D场景自适应的改变
    window.onresize = this.windowResize.bind(this);
  }
  /**
   *调整窗口尺寸
   *
   * @memberof PEBaseScene
   */
  windowResize () {
    let pixelRatio = window.devicePixelRatio;
    let width = this.rootDiv.offsetWidth * pixelRatio | 0;
    let height = this.rootDiv.offsetHeight * pixelRatio | 0;
    this.renderer.setSize(width, height, false);
    this.camera.aspect = this.rootDiv.offsetWidth / this.rootDiv.offsetHeight;
    this.camera.updateProjectionMatrix();
  }
  /**
   * 初始化一些帮助工具
   */
  InitHelp () {
    if (this.sceneConfig.isDebug) {
      this.InitAxisHelper();
      this.InitStats();
    }
  }
  /**
   *初始化辅助坐标系
   *
   * @memberof PEBaseScene
   */
  InitAxisHelper () {
    let axisHelper = new THREE.AxesHelper(1550000);
    this.scene.add(axisHelper);
  }
  /**
   *初始化性能检测组件
   *
   * @memberof PEBaseScene
   */
  InitStats () {
    this._stats = new Stats.Stats();
    this._stats.domElement.style.position = "absolute";
    this._stats.domElement.style.left = "0px";
    this._stats.domElement.style.top = "0px";
    this._stats.domElement.style.height = "50px";
    this._stats.domElement.style.zIndex = 100;
    this.rootDiv.append(this._stats.domElement);
  }
  /**
   *场景初始化完毕
   *
   * @memberof PEBaseScene
   */
  LoadingEnd () {

  }

  /**
     * 卸载模型
     *
     * @param {*} scene 当前场景
     * @param {*} geometryArray 需要卸载的模型列表
     * @memberof PEGeometry
     */
  Remove (meshArray) {
    if (meshArray.length > 0) {
      let newGeometryArray = [...meshArray];
      for (let i = 0; i < newGeometryArray.length; i++) {
        let currObj = newGeometryArray[i];
        //遍历模型中的所有子模型以及子模型上的贴图材质等信息进行手动置空删除释放
        this._deleteGroup(currObj);
        //如果场景不为空则从当前场景中删除模型显示
        if (currObj.parent != null) {
          currObj.parent.remove(currObj);
        }
      }
    }
  }

  RemoveScene () {
    //清空当前场景中缓存的所有的Object对象
    this.allMesh = new Map();
    //删除场景中所有已经添加到场景中的模型对象
    this.Remove(this.scene.children)
    //置空所有对象 为了在使用过程中不会造成内存累计增加
    this.renderer.renderLists.dispose();
    this.renderer.dispose();   
  }

  /**
       *删除group，释放内存
       *
       * @param {*} group 模型集合
       * @returns
       * @memberof PEGeometry
       */
  _deleteGroup (group) {
    let self = this;
    //console.log(group);
    if (!group) return;
    // 删除掉所有的模型组内的mesh
    group.traverse(function (item) {
      if (item.name) {
        console.log('卸载对象:' + item.name);
      }
      // console.log('Type'+item.type);
      if (item instanceof THREE.Mesh) {
        if (item.geometry != undefined) {
          item.geometry.dispose(); // 删除几何体
          item.geometry = undefined;
        }

        if (item.material) {
          if (Array.isArray(item.material)) {
            item.material.forEach(element => {
              try {
                self._deleteMaterial(element)
              } catch (error) {
                console.log(error);
              }
            });
          } else {
            try {
              self._deleteMaterial(item.material)
            } catch (error) {
              console.log(error);
            }
          }
        }
        item = undefined;
        item = null;
      }
    });
  }

  /**
   *删除模型上的材质信息
   *
   * @param {*} material
   * @memberof PEGeometry
   */
  _deleteMaterial (material) {
    if (
      material instanceof THREE.MeshFaceMaterial ||
      material instanceof THREE.MultiMaterial
    ) {
      material.materials.forEach(function (mtrl, idx) {
        if (mtrl.map) {
          mtrl.map.dispose();
          mtrl.map = undefined;
        }
        if (mtrl.lightMap) {
          mtrl.lightMap.dispose();
          mtrl.lightMap = undefined;
        }
        if (mtrl.bumpMap) {
          mtrl.bumpMap.dispose();
          mtrl.bumpMap = undefined;
        }
        if (mtrl.normalMap) {
          mtrl.normalMap.dispose();
          mtrl.normalMap = undefined;
        }
        if (mtrl.specularMap) {
          mtrl.specularMap.dispose();
          mtrl.specularMap = undefined;
        }
        if (mtrl.envMap) {
          mtrl.envMap.dispose();
          mtrl.envMap = undefined;
        }

        mtrl.dispose();
        mtrl = undefined;
      });
    } else {
      if (material.map) {
        material.map.dispose();
        material.map = undefined;
      }
      if (material.lightMap) {
        material.lightMap.dispose();
        material.lightMap = undefined;
      }
      if (material.bumpMap) {
        material.bumpMap.dispose();
        material.bumpMap = undefined;
      }
      if (material.normalMap) {
        material.normalMap.dispose();
        material.normalMap = undefined;
      }
      if (material.specularMap) {
        material.specularMap.dispose();
        material.specularMap = undefined;
      }
      if (material.envMap) {
        material.envMap.dispose();
        material.envMap = undefined;
      }

      material.dispose();
      material = undefined;
      material = null;
    }
  }

  /**
   *渲染函数
   *
   * @memberof PEBaseScene
   */
  SceneRender () {
    requestAnimationFrame(this.SceneRender.bind(this)); //请求再次执行渲染函数render
    //更新性能插件
    if (this._stats != null) {
      this._stats.update();
    }

    //更新渲染器
    this.renderer.render(
      this.scene,
      this.camera
    );
  }
}