const path = require("path");
const { name } = require("./package");

module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "../../" : "/",
  // build时构建文件的目录 构建时传入 --no-clean 可关闭该行为
  outputDir: "dist",
  // build时放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录
  assetsDir: "./",
  // 指定生成的 index.html 的输出路径 (相对于 outputDir)。也可以是一个绝对路径。
  indexPath: "index.html",
  // 默认在生成的静态资源文件名中包含hash以控制缓存
  filenameHashing: true,
  // 是否在开发环境下通过 eslint-loader 在每次保存时 lint 代码 (在生产构建时禁用 eslint-loader)
  lintOnSave: process.env.NODE_ENV !== "production",
  // 是否使用包含运行时编译器的 Vue 构建版本
  runtimeCompiler: true,
  // Babel 显式转译列表
  transpileDependencies: [],
  // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建
  productionSourceMap: true,
  // 设置生成的 HTML 中 <link rel="stylesheet"> 和 <script> 标签的 crossorigin 属性（注：仅影响构建时注入的标签）

  crossorigin: "",
  // 在生成的 HTML 中的 <link rel="stylesheet"> 和 <script> 标签上启用 Subresource Integrity (SRI)
  integrity: false,
  // 对内部的 webpack 配置（比如修改、增加Loader选项）(链式操作)

  devServer: {
    open: true,
    // host: "0.0.0.0", // 允许外部ip访问
    port: 9000, // 端口
    https: false, // 启用https
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
    proxy: {
      "/api": {
        // target: "http://192.168.1.101:8990",
        target: "http://192.168.1.195:8990",
        changeOrigin: false,
        pathRewrite: {
          "^/api": "/api",
        },
      },
      "/static": {
        // target: "http://192.168.1.101:8990",
        target: "http://192.168.1.195:8990",
        changeOrigin: false,
        pathRewrite: {
          "^/static": "/static",
        },
      },
    },
  },
  css: {
    sourceMap: true,
    loaderOptions: {
      postcss: {
        plugins: [
          require("postcss-px2rem")({
            // 设计稿为750px时为75
            // 设计图的宽度/10 比如你的设计图是1920的宽度 这里你就1920/10=192
            remUnit: 192,
          }),
        ],
      },
    },
    requireModuleExtension: true,
  },
  chainWebpack: (config) => {
    // config.module
    //   .rule("fonts")
    //   .use("url-loader")
    //   .loader("url-loader")
    //   .options({
    //     limit: 4096, // 小于4kb将会被打包成 base64
    //     fallback: {
    //       loader: "file-loader",
    //       options: {
    //         name: "fonts/[name].[hash:8].[ext]",
    //         publicPath,
    //       },
    //     },
    //   })
    //   .end();
    // config.module
    //   .rule("images")
    //   .use("url-loader")
    //   .loader("url-loader")
    //   .options({
    //     limit: 4096, // 小于4kb将会被打包成 base64
    //     fallback: {
    //       loader: "file-loader",
    //       options: {
    //         name: "img/[name].[hash:8].[ext]",
    //         publicPath,
    //       },
    //     },
    //   });
  },
  configureWebpack: {
    output: {
      library: `${name}-[name]`,
      libraryTarget: "umd", // 把微应用打包成 umd 库格式
      jsonpFunction: `webpackJsonp_${name}`,
    },
  },
  // 第三方插件配置
  pluginOptions: {},
};
